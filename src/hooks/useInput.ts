import React, { useState } from 'react';

export const useInput = (initialValues: string) => {
  const [value, setValue] = useState(initialValues);
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  return {
    value,
    onChange,
  };
};
