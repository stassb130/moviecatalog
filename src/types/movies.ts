export interface Movie {
  Title: string;
  Year: string;
  Type: string;
  imdbID: string;
  Poster: string;
}

export interface MoviesDTO {
  Search: Movie[] | [];
  totalResults: number;
  Response: 'False' | 'True';
  Error?: string;
}
