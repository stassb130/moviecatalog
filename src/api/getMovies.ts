import { API_KEY, getRequest } from './api';
import { MoviesDTO } from '../types/movies';

interface moviesParams {
  apiKey: string;
  s: string;
  page: number;
}

export const searchMovies = async (movie: string, page: number): Promise<MoviesDTO> => {
  const res = await getRequest<MoviesDTO, moviesParams>(`https://www.omdbapi.com`, {
    params: {
      apiKey: API_KEY,
      s: movie,
      page,
    },
  });
  return res.data;
};
