import axios, { AxiosResponse } from 'axios';

export const API_KEY = '8523cbb8';

export const getRequest = async <T, P>(
  url: string,
  params: { params: { s: string; apiKey: string; page: number } },
): Promise<AxiosResponse<T>> => {
  const headers = {};

  const response: AxiosResponse<T> = await axios.get<T>(url, { headers, ...params });
  return response;
};
