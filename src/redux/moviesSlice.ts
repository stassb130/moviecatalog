import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { searchMovies } from '../api/getMovies';
import { Movie, MoviesDTO } from '../types/movies';

export interface moviesState {
  movieName: string;
  movies: Movie[];
  totalResults: number;
  isLoading: boolean;
  currentPage: number;
  error: any;
}

export const fetchSearchMovies = createAsyncThunk<MoviesDTO, undefined, { state: any }>(
  'movies/fetchingSearchMovies',
  (_: undefined, thunkAPI) => {
    const { currentPage, movieName } = thunkAPI.getState().moviesReducer;
    return searchMovies(movieName, currentPage);
  },
);

const moviesSlice = createSlice({
  name: 'movies',
  initialState: {
    movieName: '',
    movies: [],
    totalResults: 0,
    isLoading: false,
    currentPage: 1,
    error: '',
  } as moviesState,
  reducers: {
    setMovieName(state, action: PayloadAction<string>) {
      state.movieName = action.payload;
      state.currentPage = 1;
    },
    setPage(state, action: PayloadAction<number>) {
      state.currentPage = action.payload;
    },
  },
  extraReducers: {
    [fetchSearchMovies.pending.type]: (state) => {
      state.isLoading = true;
      state.movies = [];
      state.error = '';
      state.totalResults = 0;
    },
    [fetchSearchMovies.fulfilled.type]: (state, { payload }: PayloadAction<MoviesDTO>) => {
      if (payload.Response === 'False') {
        state.error = payload.Error;
        state.isLoading = false;
        state.movies = [];
        state.totalResults = 0;
      } else {
        state.movies = payload.Search;
        state.isLoading = false;
        state.error = '';
        state.totalResults = payload.totalResults;
      }
    },
    [fetchSearchMovies.rejected.type]: (state, { payload }: PayloadAction<any>) => {
      state.isLoading = false;
      state.error = payload.error;
      state.movies = [];
      state.totalResults = 0;
      state.currentPage = 1;
    },
  },
});

export const { setPage, setMovieName } = moviesSlice.actions;
export default moviesSlice.reducer;
