import React from 'react';
import style from './container.module.scss';
import cx from 'classnames';

interface Props {
  className?: string;
  children: React.ReactNode;
}

const Container: React.FC<Props> = ({ children, className }) => {
  return <div className={cx(style.container, className)}>{children}</div>;
};

export { Container };
