import React from 'react';
import style from './preloader.module.scss';

const Preloader: React.FC = () => {
  return <div className={style.spinner}></div>;
};

export { Preloader };
