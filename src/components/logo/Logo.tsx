import React from 'react';
import cx from 'classnames';
import styles from './logo.module.scss';

interface Props {
  className?: string;
}

const Logo: React.FC<Props> = ({ className }) => {
  return (
    <div className={cx(styles.logo, className)}>
      <h2>Movie Catalog</h2>
    </div>
  );
};

export { Logo };
