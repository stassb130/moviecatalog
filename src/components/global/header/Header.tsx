import React from 'react';
import cx from 'classnames';
import style from './header.module.scss';
import { Logo } from '../../logo';
import { SearchBar } from '../../search-bar';
import { User } from '../../user';

interface Props {
  className?: string;
}

const Header: React.FC<Props> = ({ className }) => {
  return (
    <div className={cx(style.header, className)}>
      <Logo />
      <SearchBar />
      <User username="Aleksndr Borisenko" />
    </div>
  );
};

export { Header };
