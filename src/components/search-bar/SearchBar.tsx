import React, { useEffect } from 'react';
import style from './searchBar.module.scss';
import cx from 'classnames';
import { useInput } from '../../hooks/useInput';
import { useDebounce } from '../../hooks/useDebounce';
import { RootState, useAppDispatch } from '../../redux';
import { fetchSearchMovies, setMovieName } from '../../redux/moviesSlice';
import { useSelector } from 'react-redux';

interface Props {
  className?: string;
}

const SearchBar: React.FC<Props> = ({ className }) => {
  const movieInput = useInput('');
  const debouncedSearch = useDebounce(movieInput.value, 500);
  const dispatch = useAppDispatch();
  const movieName = useSelector<RootState, string>((state) => state.moviesReducer.movieName);

  useEffect(() => {
    dispatch(setMovieName(debouncedSearch));
  }, [debouncedSearch]);

  useEffect(() => {
    dispatch(fetchSearchMovies());
  }, [movieName]);

  return (
    <div className={cx(style.search, className)}>
      <input
        name="movie"
        type="text"
        value={movieInput.value}
        onChange={movieInput.onChange}
        placeholder="Enter movie name"
      />
    </div>
  );
};

export { SearchBar };
