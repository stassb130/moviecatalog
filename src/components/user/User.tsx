import React from 'react';
import userIcon from '../../assets/user.svg';
import downIcon from '../../assets/down-arrow.svg';
import style from './user.module.scss';
import cx from 'classnames';

interface Props {
  className?: string;
  username?: string;
}

const User: React.FC<Props> = ({ className, username }) => {
  return (
    <div className={cx(style.user, className)}>
      <img src={userIcon} alt="" />
      <h3>{username || 'unknown user'}</h3>
      <img className={style.downIcon} src={downIcon} alt="" />
    </div>
  );
};

export { User };
