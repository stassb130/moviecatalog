import React from 'react';
import leftIcon from '../../assets/left-arrow.svg';
import rightIcon from '../../assets/right-arrow.svg';
import style from './paginator.module.scss';
import cx from 'classnames';
import { createPages } from '../../utils/pagesCreator';

interface Props {
  totalResults: number;
  currentPage: number;
  callback?: (nextPage: number) => void;
}

const Paginator: React.FC<Props> = ({ totalResults, currentPage, callback }) => {
  const pagesCount = Math.ceil(totalResults / 10);
  const pages: number[] = [];
  createPages(pages, pagesCount, currentPage);
  return (
    <div className={style.paginator}>
      <img src={leftIcon} alt="" />
      {!!pages.length &&
        pages.map((page, index) => (
          <span
            className={cx(style.page, { [style.active]: page === currentPage })}
            onClick={() => callback && callback(page)}
            key={index}
          >
            {page}
          </span>
        ))}
      <img src={rightIcon} alt="" />
    </div>
  );
};

export { Paginator };
