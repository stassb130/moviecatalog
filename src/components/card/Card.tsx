import React from 'react';
import style from './card.module.scss';
import imagePlaceholder from '../../assets/placeholderimg.svg';
import cx from 'classnames';

interface Props {
  classNames?: string;
  name: string;
  year: string;
  imdbID: string;
  type: string;
  poster: string;
}

const Card: React.FC<Props> = ({ classNames, name, imdbID, type, year, poster }) => {
  const addDefaultSrc = (ev: any) => {
    ev.target.src = imagePlaceholder;
  };

  return (
    <div className={cx(style.card, classNames)}>
      <img src={poster} onError={addDefaultSrc} alt="" />
      <div className={style.description}>
        <h5>Name: {name}</h5>
        <h5>Year {year}</h5>
        <h5>imdbID: {imdbID}</h5>
        <h5>Type: {type}</h5>
      </div>
    </div>
  );
};

export { Card };
