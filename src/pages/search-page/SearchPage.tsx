import React from 'react';
import { Header } from '../../components/global/header';
import style from './searchPage.module.scss';
import cx from 'classnames';
import { Container } from '../../layouts/container';
import { MoviesBlock } from './movies-block';
import { StatusBlock } from './statuses-block';
import { SearchResultBlock } from './search-result-block';
import { SearchPaginator } from './search-paginator';

interface Props {
  className?: string;
}

const SearchPage: React.FC<Props> = ({ className }) => {
  return (
    <div className={cx(style.searchPage, className)}>
      <Header />
      <SearchResultBlock />
      <Container>
        <MoviesBlock />
      </Container>
      <StatusBlock />
      <SearchPaginator />
    </div>
  );
};

export { SearchPage };
