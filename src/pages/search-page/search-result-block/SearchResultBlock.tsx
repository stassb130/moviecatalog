import React, { useEffect } from 'react';
import style from '../searchPage.module.scss';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux';

const SearchResultBlock = () => {
  const movieName = useSelector<RootState, string>((state) => state.moviesReducer.movieName);
  const totalResults = useSelector<RootState, number>((state) => state.moviesReducer.totalResults);

  return (
    <React.Fragment>
      <h2 className={style.searchedResults}>
        {`You searched for: ${movieName.trim()}${movieName && ','} ${totalResults} results found`}
      </h2>
    </React.Fragment>
  );
};

export { SearchResultBlock };
