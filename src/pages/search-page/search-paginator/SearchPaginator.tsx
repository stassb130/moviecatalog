import React, { useEffect } from 'react';
import { Paginator } from '../../../components/paginator';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../../redux';
import { fetchSearchMovies, setPage } from '../../../redux/moviesSlice';

const SearchPaginator: React.FC = () => {
  const totalResults = useSelector<RootState, number>((state) => state.moviesReducer.totalResults);
  const currentPage = useSelector<RootState, number>((state) => state.moviesReducer.currentPage);
  const dispatch = useAppDispatch();
  const togglePageHandler = (nextPage: number) => {
    dispatch(setPage(nextPage));
  };

  useEffect(() => {
    dispatch(fetchSearchMovies());
  }, [currentPage]);

  return (
    <React.Fragment>
      {!!totalResults && (
        <Paginator
          callback={togglePageHandler}
          totalResults={totalResults}
          currentPage={currentPage}
        />
      )}
    </React.Fragment>
  );
};

export { SearchPaginator };
