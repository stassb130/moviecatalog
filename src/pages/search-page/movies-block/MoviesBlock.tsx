import React from 'react';
import { Movie } from '../../../types/movies';
import { Card } from '../../../components/card';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux';

const MoviesBlock: React.FC = () => {
  const movies = useSelector<RootState, Movie[]>((state) => state.moviesReducer.movies);
  return (
    <React.Fragment>
      {!!movies.length &&
        movies.map((data: Movie) => (
          <Card
            key={data.imdbID}
            name={data.Title}
            year={data.Year}
            type={data.Type}
            imdbID={data.imdbID}
            poster={data.Poster}
          />
        ))}
    </React.Fragment>
  );
};

export { MoviesBlock };
