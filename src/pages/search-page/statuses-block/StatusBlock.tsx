import React from 'react';
import style from '../searchPage.module.scss';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux';
import { Movie } from '../../../types/movies';
import { Preloader } from '../../../components/preloader';

const StatusBlock = () => {
  const error = useSelector<RootState, Movie[]>((state) => state.moviesReducer.error);
  const isLoading = useSelector<RootState, boolean>((state) => state.moviesReducer.isLoading);

  return (
    <React.Fragment>
      {isLoading && (
        <div className={style.statuses}>
          <Preloader />
        </div>
      )}

      {error && (
        <div className={style.statuses}>
          <div>{error}</div>
        </div>
      )}
    </React.Fragment>
  );
};
export { StatusBlock };
