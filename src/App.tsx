import React from 'react';
import './App.scss';
import { SearchPage } from './pages/search-page';

const App = () => {
  return (
    <div className="App">
      <SearchPage />
    </div>
  );
};

export default App;
